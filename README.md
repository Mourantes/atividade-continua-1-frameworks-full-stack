# Atividade Contínua 1 - Frameworks Full Stack

## Motivo do Repositório
Este é um repositório para armazenar a Atividade Contínua 1 da matéria de Frameworks Fullsrack do curso de Sistemas da informação da Faculdade IMPACTA de Tecnologia.

# Instruções da Atividade
Criar com o Flask, uma um backend que contenha uma rota, essa rota deverá receber 2 parâmetros ( duas notas do aluno) e exibir essa nota com a media aplicada.

Exemplo: nota1 + nota2 /2

# Como testar o código
Necessário ter o Flask instalado
Ao rodar o código pegar a url da porta utilizada e colocar na barra de pesquisa, ou ferramenta
Colocar depois da url utilizando o método GET: /?nota1=[primeiro valor]&nota2=[segundo valor]

## Informações do Aluno
Nome: Murilo Rodrigues Moura
RA: 2102198
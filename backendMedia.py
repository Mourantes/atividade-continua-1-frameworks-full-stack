from flask import Flask, request #importa biblioteca

app = Flask(__name__) # inicializa a aplicação

@app.route('/') #cria rota
def main():
    resultado = 'Entre as notas na URL'
    # Para ser realizado a operação, deve usar o método na URL, /?nota1=x&nota2=y.

    nota1 = request.args.get('nota1')
    nota2 = request.args.get('nota2')

    if nota1 and nota2:
        nota1 = float(nota1)
        nota2 = float(nota2)

        media = (nota1 + nota2) / 2
        resultado = 'A média das notas é: ' + str(media)
    
    return resultado

if __name__ == '__main__':
    app.run(debug=True) #Executa a aplicação
